import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../../app/services/notification.service';
import {ConfigurationService} from '../services/configuration.service';

import * as moment from 'moment';
import {NgbCalendar, NgbDate} from "@ng-bootstrap/ng-bootstrap";
import {Moment} from "moment";

@Component({
    selector: 'app-transaction',
    templateUrl: './transaction.component.html',
    styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {

    rows: any = [];
    config: any = [];
    currentPage = 0;
    pageSize = 5;
    totalSize;
    display = 'none';
    filterData: any = {};
    selectedDate: { start: Moment, end: Moment };
    limitOptions = [
        {
            key: '5',
            value: 5
        },
        {
            key: '10',
            value: 10
        },
        {
            key: '20',
            value: 20
        }
    ];
    fromDate: NgbDate;
    toDate: NgbDate;
    conditionAry: any = ['refer', 'deposit', 'reward', 'revoke'];

    constructor(private notificationService: NotificationService,
                private configurationService: ConfigurationService,
                calendar: NgbCalendar) {
        this.fromDate = calendar.getToday();
        this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
    }

    ngOnInit() {
        this.getTransactionList(this.currentPage, this.pageSize);
    }

    getTransactionList(pageNumber, pageSize) {
        console.log('filterData.', this.filterData);
        const data = {
            pg: pageNumber,
            pgSize: pageSize,
            data: this.filterData
        }
        this.configurationService.getTransactionList(data)
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response.data);
                        this.rows = response.data.results;
                        this.totalSize = response.data.count;
                    } else {
                        this.rows = [];
                        this.totalSize = 0;
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    filterMethod() {
        if (this.selectedDate.start !== null) {
            this.filterData.startDate = moment(this.selectedDate.start).format('YYYY-MM-DD');
        }
        if (this.selectedDate.end !== null) {
            this.filterData.endDate = moment(this.selectedDate.end).format('YYYY-MM-DD');
        }
        this.getTransactionList(this.currentPage, this.pageSize);
    }

    onPageSizeChanged(event) {
        this.currentPage = 0;
        this.pageSize = event;
        this.getTransactionList(this.currentPage, this.pageSize);
    }

    pageCallback(e) {
        this.getTransactionList(e.offset, e.pageSize);
    }

    revokeData(row) {
        console.log('row', row);
        const body = {
            competitionId: row.competitionId,
            playerId: row.playerId,
            transactionId: row.transactionId,
            transactionAmount: row.transactionAmount
        }
        this.configurationService.revokeData(body)
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response.data);
                        this.notificationService.showNotification(response.message, 'success')
                        this.getTransactionList(this.currentPage, this.pageSize);
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    revokeTransaction(row) {
        console.log('row', row);
        const body = {
            playerId: row.playerId,
            transactionId: row.transactionId,
            currencyAmount: row.currencyAmount
        }
        console.log('body', body);
        this.configurationService.revokeTransaction(body)
            .subscribe((response) => {
                console.log('response', response)
                    if (!response.error) {
                        console.log('response.data.results', response.data);
                        this.notificationService.showNotification(response.message, 'success')
                        this.getTransactionList(this.currentPage, this.pageSize);
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }
}