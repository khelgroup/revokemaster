const express = require('express');
const router = express.Router();
const competitionEntryController = require("../controllers/competitionEntryController");
const appConfig = require("./../../config/appConfig");

module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/competitionEntry`;

    // defining routes.

    // create player
    app.post(`${baseUrl}/`, competitionEntryController.createRecord);

}
