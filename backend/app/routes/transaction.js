const express = require('express');
const router = express.Router();
const transactionController = require("./../../app/controllers/transactionController");
const appConfig = require("./../../config/appConfig");

module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/transaction`;

    // defining routes.

    // create player
    app.post(`${baseUrl}/`, transactionController.getFilterTransaction);

    app.get(`${baseUrl}/revoke`, transactionController.transactionRevoke);

}
