const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib');
const logger = require('./../libs/loggerLib');
const check = require('../libs/checkLib');
const tokenLib = require('../libs/tokenLib');


var faker = require('faker');

const Competition = mongoose.model('competition');
const CompetitionEntry = mongoose.model('CompetitionEntry');
const Player = mongoose.model('Player');

let getCompetitionData = (req, res) => {

    // get size Limit
    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.body.pgSize) ? Number(req.body.pgSize) : 5;
            if (req.body.pg > 0) {
                skip = (limit) * (req.body.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    let findAllRecords = (data) => {
        console.log("findAllRecords");
        return new Promise((resolve, reject) => {
            let body = {};
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.id !== undefined) {
                    body['competitionId'] = (req.body.data.id);
                }
                if (req.body.data.startDate && req.body.data.endDate) {
                    body["createdOn"] = {
                        "$gte": new Date(req.body.data.startDate),
                        "$lt": new Date(req.body.data.endDate)
                    }
                }
            }
            console.log('body', body);
            Competition.find(body, {}, {
                skip: data.skip,
                limit: data.limit,
                sort: {createdOn: -1}
            })
                .select('-__v -_id')
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to find PlayerHistory", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("No rewards found", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "No Competition found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findAllRecords function

    let findAllRecordscount = (data) => {
        console.log("findAllRecordscount");
        return new Promise((resolve, reject) => {
            let body = {};
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.id !== undefined) {
                    body['competitionId'] = (req.body.data.id);
                }
                if (req.body.data.startDate && req.body.data.endDate) {
                    body["createdOn"] = {
                        "$gte": new Date(req.body.data.startDate),
                        "$lt": new Date(req.body.data.endDate)
                    }
                }
            }
            Competition.find(body)
                .count()
                .exec((err, rewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = {
                            results: data,
                            count: rewardsDetails
                        }
                        resolve(final);
                    }
                });
        });
    } // end of findAllRecordscount function

    getSizeLimit()
        .then(findAllRecords)
        .then(findAllRecordscount)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Competition Data Successfully!!", 200, resolve);
            console.log('res', apiResponse);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of getCompetitionData function

let getTransaction = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.query.id) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "id is missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    // get size Limit
    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
            if (req.query.pg > 0) {
                skip = (limit) * (req.query.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    let findCompetitionOrPlayer = (data) => {
        console.log("findCompetitionOrPlayer");
        return new Promise((resolve, reject) => {
            if (req.query.type === 'CO') {
                Competition.findOne({competitionId: req.query.id}, function (err, tokenDetails) {
                    if (err) {
                        logger.error("Failed to insert PlayerHistory", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to insert PlayerHistory", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(tokenDetails)) {
                        logger.error("competition not found", "playerController => findCompetitionOrPlayer()", 5);
                        let apiResponse = response.generate(true, "competition not found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(data);
                    }
                });
            } else {
                Player.findOne({playerId: req.query.id}, function (err, tokenDetails) {
                    if (err) {
                        logger.error("Failed to insert PlayerHistory", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to insert PlayerHistory", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(tokenDetails)) {
                        logger.error("Player not found", "playerController => findCompetitionOrPlayer()", 5);
                        let apiResponse = response.generate(true, "Player not found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(data);
                    }
                });
            }
        });
    } // end of findCompetitionOrPlayer function

    let findTransaction = (data) => {
        console.log("findTransaction");
        return new Promise((resolve, reject) => {
            let body = {};
            if (req.query.type === 'CO') {
                body = {competitionId: req.query.id, revokeStatus: false};
            } else {
                body = {playerId: req.query.id, revokeStatus: false};
            }
            CompetitionEntry.find(body, {}, {
                skip: data.skip,
                limit: data.limit,
                sort: {createdOn: -1}
            })
                .select('-__v -_id')
                .exec((err, rewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("Transaction not found for this competition or player", "playerController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Transaction not found for this competition or player", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findTransaction function

    let findPlayerName = (data) => {
        console.log("findPlayerName");
        return new Promise((resolve, reject) => {
            let playerIds = [];
            data.filter((x) => {
                if (playerIds.indexOf(x.playerId) === -1) {
                    playerIds.push(x.playerId);
                }
            });
            Player.find({playerId: {"$in": playerIds}})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("Transaction not found", "playerController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Transaction not found", 500, null);
                        reject(apiResponse);
                    } else {
                        let result = [];
                        rewardsDetails.filter((x) => {
                            x = x.toObject();
                            data.filter((y) => {
                                y = y.toObject();
                                if (x.playerId === y.playerId) {
                                    y['profileName'] = x.profileName;
                                    result.push(y);
                                }
                            });
                        });
                        resolve(result);
                    }
                });
        });
    } // end of findPlayerName function

    let findCompetitionName = (data) => {
        console.log("findCompetitionName");
        return new Promise((resolve, reject) => {
            let competitionIds = [];
            data.filter((x) => {
                if (competitionIds.indexOf(x.competitionId) === -1) {
                    competitionIds.push(x.competitionId);
                }
            });
            Competition.find({competitionId: {"$in": competitionIds}})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("Transaction not found", "playerController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Transaction not found", 500, null);
                        reject(apiResponse);
                    } else {
                        let result = [];
                        rewardsDetails.filter((x) => {
                            x = x.toObject();
                            data.filter((y) => {
                                if (x.competitionId === y.competitionId) {
                                    y['competitionName'] = x.competitionName;
                                    result.push(y);
                                }
                            });
                        });
                        resolve(result);
                    }
                });
        });
    } // end of findCompetitionName function

    let transactionCount = (data) => {
        console.log("transactionCount");
        return new Promise((resolve, reject) => {
            let body = {};
            if (req.query.type === 'CO') {
                body = {competitionId: req.query.id, revokeStatus: false};
            } else {
                body = {playerId: req.query.id, revokeStatus: false};
            }
            CompetitionEntry.find(body)
                .count()
                .exec((err, rewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("Transaction not found", "playerController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Transaction not found", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = {
                            results: data,
                            count: rewardsDetails
                        }
                        resolve(final);
                    }
                });
        });
    } // end of transactionCount function

    validatingInputs()
        .then(getSizeLimit)
        .then(findCompetitionOrPlayer)
        .then(findTransaction)
        .then(findPlayerName)
        .then(findCompetitionName)
        .then(transactionCount)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Transaction Data Successfully!!", 200, resolve);
            console.log('res', apiResponse);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of getTransaction function


module.exports = {
    getCompetitionData: getCompetitionData,
    getTransaction: getTransaction,
}// end exports
