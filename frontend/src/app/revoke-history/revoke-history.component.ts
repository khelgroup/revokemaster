import {Component, OnInit} from '@angular/core';
import {NgbCalendar, NgbDate} from "@ng-bootstrap/ng-bootstrap";
import {NotificationService} from "../services/notification.service";
import {ConfigurationService} from "../services/configuration.service";

@Component({
    selector: 'app-revoke-history',
    templateUrl: './revoke-history.component.html',
    styleUrls: ['./revoke-history.component.scss']
})
export class RevokeHistoryComponent implements OnInit {

    rows: any = [];
    config: any = [];
    currentPage = 0;
    pageSize = 5;
    totalSize;
    display = 'none';
    filterData: any = {};
    limitOptions = [
        {
            key: '5',
            value: 5
        },
        {
            key: '10',
            value: 10
        },
        {
            key: '20',
            value: 20
        }
    ];
    hoveredDate: NgbDate;
    mydate: any = '';
    fromDate: NgbDate;
    toDate: NgbDate;

    constructor(private notificationService: NotificationService,
                private configurationService: ConfigurationService,
                calendar: NgbCalendar) {
        this.fromDate = calendar.getToday();
        this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
    }

    ngOnInit() {
        this.getTransactionList(this.currentPage, this.pageSize);
    }

    getTransactionList(pageNumber, pageSize) {
        console.log('filterData.', this.filterData);
        const data = {
            pg: pageNumber,
            pgSize: pageSize,
            data: this.filterData
        }
        this.configurationService.getRevokeHistoryList(pageNumber, pageSize)
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response.data);
                        this.rows = response.data.results;
                        this.totalSize = response.data.count;
                    } else {
                        this.rows = [];
                        this.totalSize = 0;
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    filterMethod() {
        if (this.filterData.startDate && this.filterData.endDate) {
            if (new Date(this.filterData.startDate).getTime() < new Date(this.filterData.endDate).getTime()) {
                this.notificationService.showNotification('Perfect Date Format', 'success');
                this.getTransactionList(this.currentPage, this.pageSize);
            } else {
                delete this.filterData['startDate'];
                delete this.filterData['endDate'];
                this.notificationService.showNotification('Select Proper Date Format', 'danger');
            }
        } else {
            if (this.filterData.startDate && !this.filterData.endDate) {
                this.notificationService.showNotification('Select End Date', 'danger');
            } else if (this.filterData.endDate && !this.filterData.startDate) {
                this.notificationService.showNotification('Select Start Date', 'danger');
            } else if (this.filterData.amountFilter) {
                this.getTransactionList(this.currentPage, this.pageSize);
            }
        }

    }

    onPageSizeChanged(event) {
        this.currentPage = 0;
        this.pageSize = event;
        this.getTransactionList(this.currentPage, this.pageSize);
    }

    pageCallback(e) {
        this.getTransactionList(e.offset, e.pageSize);
    }

    revokeData(row) {
        console.log('row', row);
        const body = {
            competitionId: row.competitionId,
            playerId: row.playerId,
            transactionId: row.transactionId,
            transactionAmount: row.transactionAmount
        }
        this.configurationService.revokeData(body)
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response.data);
                        this.notificationService.showNotification(response.message, 'success')
                        this.getTransactionList(this.currentPage, this.pageSize);
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

}
