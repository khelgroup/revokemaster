'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let competitionSchema = new Schema({
    competitionId: {
        type: String,
        default: '',
// enables us to search the record faster
        index: true,
        unique: true
    },
    // playerId
    competitionName: {
        type: String,
        default: ''
    },
    // rewardMasterId of rewardMaster
    competitionType: {
        type: String,
        default: ''
    },
    // rewardAmount like 10 coins/ points/ etc
    gameId: {
        type: String,
        default: ''
    },
    description: {
        type: String,
        default: ''
    },
    entryType: {
        type: String,
        default: ''
    },
    winningBreakUp: {
        type: Array,
        default: []
    },
    scores: {
        type: Array,
        default: []
    },
    ranks: {
        type: Array,
        default: []
    },
    status: {
        type: String,
        default: ''
    },
    productId: {
        type: String,
        default: ''
    },
    startTime: {
        type: Date,
        default: ''
    },
    playerLimit: {
        type: Number,
        default: 0
    },
    winningAmount: {
        type: Number,
        default: 0
    },
    entryAmount: {
        type: Number,
        default: 0
    },
    createdOn: {
        type: Date,
        default: ''
    },
    endTime: {
        type: Date,
        default: ''
    },
}, {versionKey: false});

mongoose.model('competition', competitionSchema);
