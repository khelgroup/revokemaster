'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let CompetitionEntryCollectionSchema = new Schema({
    ceId: {
        type: String,
        default: '',
        index: true,
        unique: true
    },
    // currencyType: {
    //     type: String,
    //     default: ''
    // },
    entryAmount: {
        type: Number,
        default: 0
    },
    competitionId: {
        type: String,
        default: '',
        index: true
    },
    winningCashBalance: {
        type: Number,
        default: 0
    },
    bonusCashBalance: {
        type: Number,
        default: 0
    },
    depositCashBalance: {
        type: Number,
        default: 0
    },
    playerId: {
        type: String,
        default: '',
        index: true
    },
    revokeStatus:{
        type: Boolean,
        default: false
    },
    createdOn: {
        type: Date,
        default: ''
    }
}, {versionKey: false})


mongoose.model('CompetitionEntry', CompetitionEntryCollectionSchema);
