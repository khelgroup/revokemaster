const express = require('express');
const router = express.Router();
const revokeController = require("./../../app/controllers/revokeController");
const appConfig = require("./../../config/appConfig");

module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/revoke`;

    // defining routes.

    // create player
    app.post(`${baseUrl}/`, revokeController.revokeTransaction);
    app.post(`${baseUrl}/all`, revokeController.revokeAllTransaction);
    app.get(`${baseUrl}/`, revokeController.revokeHistory);

}
