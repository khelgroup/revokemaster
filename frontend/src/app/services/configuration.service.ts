import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ConfigurationService {

    api = environment.apiUrl;

    constructor(private http: HttpClient) {
    }

    login(data) {
        return this.http.post<any>(this.api + '/users/loginUser', data);
    }

    logoutUser(data) {
        return this.http.post<any>(this.api + '/users/logoutUser', data);
    }

    getAdminRewards(pageNumber, pageSize, userId) {
        return this.http.get<any>(this.api + '/rewards?userId=' + userId + '&pg=' + pageNumber + '&pgSize=' + pageSize);
    }

    getCompetitionData(data) {
        return this.http.post<any>(this.api + '/competition', data);
    }

    getPlayerList(data) {
        return this.http.post<any>(this.api + '/players/list', data);
    }

    getRevokeHistoryList(pageNumber, pageSize) {
        return this.http.get<any>(this.api + '/revoke?pg=' + pageNumber + '&pgSize=' + pageSize);
    }

    updatePlayerWallet(data) {
        return this.http.post<any>(this.api + '/players/update', data);
    }
    deductPlayerWallet(data) {
        return this.http.post<any>(this.api + '/players/deduct', data);
    }

    getTransactionDataById(pageNumber, pageSize, id, type) {
        return this.http.get<any>(this.api + '/competition/getTransaction?id=' + id + '&type=' + type + '&pg=' + pageNumber + '&pgSize=' + pageSize);
    }

    getTransactionList(data) {
        return this.http.post<any>(this.api + '/transaction/', data);
    }

    revokeData(data) {
        return this.http.post<any>(this.api + '/revoke', data);
    }

    revokeTransaction(data) {
        return this.http.get<any>(this.api + '/transaction/revoke?playerId=' + data.playerId + '&transactionId=' + data.transactionId + '&currencyAmount=' + data.currencyAmount);
    }

    revokeallData(data) {
        return this.http.post<any>(this.api + '/revoke/all', data);
    }

    getallRewards() {
        return this.http.get<any>(this.api + '/rewards/all');
    }

    getPlayerRewardHistory(pageNumber, pageSize, userId) {
        return this.http.get<any>(this.api + '/players/getclaimrewards?userId=' + userId + '&pg=' + pageNumber + '&pgSize=' + pageSize);
    }

    getAllRewardsForPlayers(pageNumber, pageSize, userId) {
        return this.http.get<any>(this.api + '/players/getallrewards?userId=' + userId + '&pg=' + pageNumber + '&pgSize=' + pageSize);
    }

    getPlayerInfo(userId) {
        return this.http.get<any>(this.api + '/players/getplayerinfo?userId=' + userId);
    }

    updatePlayerProfile(data) {
        return this.http.post<any>(this.api + '/players/update', data);
    }

    addRewards(data) {
        return this.http.post<any>(this.api + '/rewards', data);
    }


    // ============================

    deleteConfiguration(data) {
        return this.http.post<any>(this.api + '/configuration/delete', data);
    }

    getTransaction(pageNumber, pageSize, userId, date) {
        return this.http.get<any>(this.api + '/users/getTransaction?configurationName=' + userId + '&pg=' + pageNumber + '&pgSize=' + pageSize + '&date=' + date);
    }

    getConfigurationIds(userId) {
        return this.http.get<any>(this.api + '/configuration/getConfigurationIds?userId=' + userId);
    }

}
