import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {RouterModule} from '@angular/router';

import {AppRoutingModule} from './app.routing';
import {NavbarModule} from './shared/navbar/navbar.module';
import {FooterModule} from './shared/footer/footer.module';
import {SidebarModule} from './sidebar/sidebar.module';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';

import {AppComponent} from './app.component';
import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {HttpClientModule} from '@angular/common/http';
import {ToastrModule} from 'ngx-toastr';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        RouterModule,
        HttpModule,
        NavbarModule,
        FooterModule,
        SidebarModule,
        AppRoutingModule,
        HttpClientModule,
        ToastrModule.forRoot(),
        NgxDatatableModule,
        NgxDaterangepickerMd.forRoot(),
    ],
    declarations: [
        AppComponent,
        AdminLayoutComponent,
    ],
    providers: [ ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
