import {Component, OnInit} from '@angular/core';
import {NotificationService} from "../services/notification.service";
import {ConfigurationService} from "../services/configuration.service";

@Component({
    selector: 'app-players',
    templateUrl: './players.component.html',
    styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {

    rows: any = [];
    details: any = [];
    config: any = [];
    currentPage = 0;
    detailscurrentPage = 0;
    pageSize = 5;
    totalSize;
    detailstotalSize;
    display = 'none';
    display1 = 'none';
    display2 = 'none';
    editBlock = false;
    configurationData: any = {};
    updateWallet: any = {};
    filterData: any = {};
    limitOptions = [
        {
            key: '5',
            value: 5
        },
        {
            key: '10',
            value: 10
        },
        {
            key: '20',
            value: 20
        }
    ];


    constructor(private notificationService: NotificationService,
                private configurationService: ConfigurationService) {
    }

    ngOnInit() {
        this.getPlayerList(this.currentPage, this.pageSize);
    }

    getPlayerList(pageNumber, pageSize) {
        const data = {
            pg: pageNumber,
            pgSize: pageSize,
            data: this.filterData
        };
        this.configurationService.getPlayerList(data)
            .subscribe((response) => {
                    if (!response.error) {
                        this.filterData = {};
                        console.log('response.data.results', response.data);
                        this.rows = response.data.results;
                        this.totalSize = response.data.count;
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    detailData(pageNumber, pageSize, data) {
        this.configurationService.getTransactionDataById(pageNumber, pageSize, data.playerId, 'PL')
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response.data);
                        this.details = response.data.results;
                        this.detailstotalSize = response.data.count;
                    } else {
                        this.editBlock = false;
                        this.display = 'none';
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    showDetails(data) {
        this.editBlock = false;
        this.display = 'block';
        this.detailData(this.detailscurrentPage, this.pageSize, data)
    }

    personalRevoke(data) {
        this.editBlock = false;
        this.display1 = 'block';
        this.updateWallet.playerId = data.playerId;
    }

    personalAdminRevoke(data) {
        this.editBlock = false;
        this.display2 = 'block';
        this.updateWallet.playerId = data.playerId;
    }

    onCloseHandled() {
        this.display = 'none';
        this.display1 = 'none';
        this.configurationData = {};
    }

    filterMethod() {
        this.getPlayerList(this.currentPage, this.pageSize);
    }

    saveConfiguration() {
        console.log('this.updateWallet', this.updateWallet);
        this.configurationService.updatePlayerWallet(this.updateWallet)
            .subscribe((response) => {
                    if (!response.error) {
                        this.display1 = 'none';
                        this.updateWallet = {};
                        this.notificationService.showNotification(response.message, 'success')

                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    saveRerevokeConfiguration() {
        console.log('this.updateWallet', this.updateWallet);
        this.configurationService.deductPlayerWallet(this.updateWallet)
            .subscribe((response) => {
                    if (!response.error) {
                        this.display2 = 'none';
                        this.updateWallet = {};
                        this.notificationService.showNotification(response.message, 'success')

                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    onPageSizeChanged(event) {
        this.currentPage = 0;
        this.pageSize = event;
        this.getPlayerList(this.currentPage, this.pageSize);
    }

    pageCallback(e) {
        this.getPlayerList(e.offset, e.pageSize);
    }

    detailPageCallback(e, row) {
        console.log('e.competitionId', e);
        console.log('e.competitionId', row[0].competitionId);
        this.detailData(e.offset, e.pageSize, row[0]);
    }

    revokeData(row) {
        console.log('row', row);
        const body = {
            competitionId: row.competitionId,
            playerId: row.playerId,
            ceId: row.ceId,
            entryAmount: row.entryAmount,
            depositCashBalance: row.depositCashBalance,
            bonusCashBalance: row.bonusCashBalance,
            winningCashBalance: row.winningCashBalance
        }
        this.configurationService.revokeData(body)
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response.data);
                        this.notificationService.showNotification(response.message, 'success')
                        this.detailData(this.detailscurrentPage, this.pageSize, row)
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

}
