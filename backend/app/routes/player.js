const express = require('express');
const router = express.Router();
const playerController = require("./../../app/controllers/playerController");
const appConfig = require("./../../config/appConfig");

module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/players`;

    // defining routes.

    // create player
    app.post(`${baseUrl}/list`, playerController.list);

    app.post(`${baseUrl}/update`, playerController.updatePlayerWallet);

    app.post(`${baseUrl}/deduct`, playerController.deductPlayerWallet);

}
