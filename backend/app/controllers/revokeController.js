const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib');
const logger = require('./../libs/loggerLib');
const check = require('../libs/checkLib');

const Competition = mongoose.model('competition');
const Transaction = mongoose.model('transaction');
const Player = mongoose.model('Player');
const CompetitionEntry = mongoose.model('CompetitionEntry');
const RevokeHistory = mongoose.model('revokeHistory');
const Wallet = mongoose.model('Wallet');

let revokeTransaction = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.competitionId && req.body.playerId && req.body.ceId && req.body.entryAmount) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "competitionId or playerId or ceId or entryAmount is missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let findPlayer = () => {
        console.log("findPlayer");
        return new Promise((resolve, reject) => {
            Player.findOne({playerId: req.body.playerId}, {}, {})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to find Player", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find Player", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("No Player found", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "No Player found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findPlayers function

    let findCompetition = () => {
        console.log("findCompetition");
        return new Promise((resolve, reject) => {
            Competition.findOne({competitionId: req.body.competitionId}, {}, {})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to find Competition", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find Competition", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("No Competition found", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "No Competition found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findPlayers function

    let findTransaction = () => {
        console.log("findTransaction");
        return new Promise((resolve, reject) => {
            CompetitionEntry.findOne({ceId: req.body.ceId}, {}, {})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to find Transaction", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find Transaction", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("No Transaction found", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "No Transaction found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findPlayers function

    let updateWallet = () => {
        console.log("updateWallet", req.body.transactionAmount, req.body.playerId);
        return new Promise((resolve, reject) => {
            Wallet.findOneAndUpdate({playerId: req.body.playerId}, {
                $inc: {
                    depositCashBalance: req.body.depositCashBalance,
                    bonusCashBalance: req.body.bonusCashBalance,
                    winningCashBalance: req.body.winningCashBalance,
                    totalCashBalance: req.body.entryAmount
                }
            }, {new: true})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to update wallet", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to update wallet", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findPlayers function

    let createTransaction = (body) => {
        console.log("createTransaction");
        return new Promise((resolve, reject) => {
            body['transactionId'] = shortid.generate();
            body['createdOn'] = new Date();
            Transaction.create(body, function (err, TransactionDetails) {
                if (err) {
                    logger.error("Failed to create Transaction", "playerController => insertPlayerReward()", 5);
                    let apiResponse = response.generate(true, "Failed to create Transaction", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(TransactionDetails);
                }
            });
        });
    } // end of findPlayerWallet function

    let revokeHistory = () => {
        console.log("revokeHistory");
        return new Promise((resolve, reject) => {
            let body = {};
            body['revokeHistoryId'] = shortid.generate();
            body['playerId'] = req.body.playerId;
            body['competitionId'] = req.body.competitionId;
            body['transactionId'] = req.body.ceId;
            body['transactionAmount'] = req.body.entryAmount;
            body['createdOn'] = new Date();
            RevokeHistory.create(body, function (err, rewardsDetails) {
                if (err) {
                    console.log('err', err)
                    logger.error("Failed to Create", "playerController => insertPlayerReward()", 5);
                    let apiResponse = response.generate(true, "Failed to Create", 500, null);
                    reject(apiResponse);
                } else {
                    let newBody = {
                        playerId: req.body.playerId,
                        currencyType: 'deposit',
                        currencyAmount: req.body.entryAmount,
                        transactionType: 'revoke',
                        transactionName: 'revoke Amount of joining competition'
                    }
                    createTransaction(newBody)
                        .then(() => {
                            resolve(rewardsDetails);
                        })
                }
            });
        });
    } // end of findPlayers function

    let updateTransaction = () => {
        console.log("updateTransaction");
        return new Promise((resolve, reject) => {
            CompetitionEntry.findOneAndUpdate({ceId: req.body.ceId}, {revokeStatus: true})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to Create", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to Create", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findPlayers function


    validatingInputs()
        .then(findPlayer)
        .then(findCompetition)
        .then(findTransaction)
        .then(updateWallet)
        .then(revokeHistory)
        .then(updateTransaction)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Update Successfully!!", 200, resolve);
            console.log('res', apiResponse);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of revokeTransaction function

let revokeAllTransaction = (req, res) => {

    let porccessData = () => {
        console.log("porccessData");
        return new Promise((resolve, reject) => {
            console.log('data', req.body.length);
            console.log('data', req.body);
            (req.body).forEach((x) => {
                findPlayer(x.playerId)
                    .then(findCompetition(x.competitionId))
                    .then(findTransaction(x.ceId))
                    .then(() => {
                        let newBody = {
                            playerId: x.playerId,
                            depositCashBalance: x.depositCashBalance,
                            bonusCashBalance: x.bonusCashBalance,
                            winningCashBalance: x.winningCashBalance,
                            entryAmount: x.entryAmount
                        }
                        updateWallet(newBody)
                            .then(() => {
                                let body = {
                                    playerId: x.playerId,
                                    competitionId: x.competitionId,
                                    ceId: x.ceId,
                                    entryAmount: x.entryAmount
                                }
                                revokeHistory(body)
                                    .then(updateTransaction(x.ceId))
                                    .then(resolve)
                            })
                    })
            })
        });
    }

    let findPlayer = (id) => {
        console.log("findPlayer");
        return new Promise((resolve, reject) => {
            Player.findOne({playerId: id}, {}, {})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to find Player", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find Player", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("No Player found", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "No Player found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findPlayers function

    let findCompetition = (id) => {
        console.log("findCompetition");
        return new Promise((resolve, reject) => {
            Competition.findOne({competitionId: id}, {}, {})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to find Competition", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find Competition", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("No Competition found", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "No Competition found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findPlayers function

    let findTransaction = (id) => {
        console.log("findTransaction");
        return new Promise((resolve, reject) => {
            CompetitionEntry.findOne({ceId: id}, {}, {})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to find Transaction", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find Transaction", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("No Transaction found", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "No Transaction found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findPlayers function

    let updateWallet = (body) => {
        console.log("updateWallet");
        return new Promise((resolve, reject) => {
            Wallet.findOneAndUpdate({playerId: body.playerId}, {
                $inc: {
                    depositCashBalance: body.depositCashBalance,
                    bonusCashBalance: body.bonusCashBalance,
                    winningCashBalance: body.winningCashBalance,
                    totalCashBalance: body.entryAmount
                }
            }, {new: true})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to update wallet", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to update wallet", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findPlayers function

    let createTransaction = (body) => {
        console.log("createTransaction");
        return new Promise((resolve, reject) => {
            body['transactionId'] = shortid.generate();
            body['createdOn'] = new Date();
            Transaction.create(body, function (err, TransactionDetails) {
                if (err) {
                    logger.error("Failed to create Transaction", "playerController => insertPlayerReward()", 5);
                    let apiResponse = response.generate(true, "Failed to create Transaction", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(TransactionDetails);
                }
            });
        });
    } // end of findPlayerWallet function

    let revokeHistory = (data) => {
        console.log("revokeHistory");
        return new Promise((resolve, reject) => {
            let body = {};
            body['revokeHistoryId'] = shortid.generate();
            body['playerId'] = data.playerId;
            body['competitionId'] = data.competitionId;
            body['transactionId'] = data.ceId;
            body['transactionAmount'] = data.entryAmount;
            body['createdOn'] = new Date();
            RevokeHistory.create(body, function (err, rewardsDetails) {
                if (err) {
                    console.log('err', err)
                    logger.error("Failed to Create", "playerController => insertPlayerReward()", 5);
                    let apiResponse = response.generate(true, "Failed to Create", 500, null);
                    reject(apiResponse);
                } else {
                    let newBody = {
                        playerId: data.playerId,
                        currencyType: 'deposit',
                        currencyAmount: data.entryAmount,
                        transactionType: 'revoke',
                        transactionName: 'revoke Amount of joining competition'
                    }
                    createTransaction(newBody)
                        .then(() => {
                            resolve(rewardsDetails);
                        })
                }
            });
        });
    } // end of findPlayers function

    let updateTransaction = (id) => {
        console.log("updateTransaction");
        return new Promise((resolve, reject) => {
            CompetitionEntry.findOneAndUpdate({ceId: id}, {revokeStatus: true})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to Create", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to Create", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findPlayers function

    porccessData()
        .then((resolve) => {
            let apiResponse = response.generate(false, "Update Successfully!!", 200, resolve);
            console.log('res', apiResponse);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of revokeAllTransaction function

let revokeHistory = (req, res) => {

    // get size Limit
    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
            if (req.query.pg > 0) {
                skip = (limit) * (req.query.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    let findHistory = (data) => {
        console.log("findHistory");
        return new Promise((resolve, reject) => {
            RevokeHistory.find({}, {}, {
                skip: data.skip,
                limit: data.limit,
                sort: {createdOn: -1}
            })
                .select('-__v -_id')
                .exec((err, playerDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve players", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(playerDetails)) {
                        logger.error("No History found", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "No Revoke History Found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(playerDetails);
                    }
                })
        });
    }

    let findCompetition = (data) => {
        console.log("findCompetition");
        return new Promise((resolve, reject) => {
            let ids = [];
            data.filter((x) => {ids.push(x.competitionId)});
            Competition.find({competitionId: {$in: ids}})
                .select('-__v -_id')
                .exec((err, playerDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve players", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(playerDetails)) {
                        logger.error("No transaction found", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "No transaction Found", 500, null);
                        reject(apiResponse);
                    } else {
                        let result = [];
                        data.filter((x)=>{
                            playerDetails.filter((y)=> {
                                y = y.toObject();
                                if(x.competitionId === y.competitionId){
                                    x['competitionName'] = y.competitionName;
                                    result.push(x);
                                }
                            })
                        })
                        resolve(result);
                    }
                })
        });
    }

    let finaHistorycount = (data) => {
        console.log("finaHistorycount");
        return new Promise((resolve, reject) => {
            RevokeHistory.find({})
                .count()
                .exec((err, rewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = {
                            results: data,
                            count: rewardsDetails
                        }
                        resolve(final);
                    }
                });
        });
    } // end of findPlayers function

    getSizeLimit()
        .then(findHistory)
        .then(findCompetition)
        .then(finaHistorycount)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Revoke History List Successfully!!", 200, resolve);
            console.log('res', apiResponse);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of revokeHistory function

module.exports = {
    revokeTransaction: revokeTransaction,
    revokeAllTransaction: revokeAllTransaction,
    revokeHistory: revokeHistory,
}// end exports
