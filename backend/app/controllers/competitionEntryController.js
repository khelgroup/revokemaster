const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib');
const logger = require('./../libs/loggerLib');
const check = require('../libs/checkLib');
const tokenLib = require('../libs/tokenLib');

var faker = require('faker');

/* Models */

const CompetitionEntry = mongoose.model('CompetitionEntry');
const Transaction = mongoose.model('transaction');
const Competition = mongoose.model('competition');
const Player = mongoose.model('Player');
const Wallet = mongoose.model('Wallet');

let createRecord = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.playerId && req.body.competitionId) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "playerId or competitionId is missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let findCompetition = () => {
        console.log("findCompetition");
        return new Promise((resolve, reject) => {
            Competition.findOne({competitionId: req.body.competitionId}, function (err, tokenDetails) {
                if (err) {
                    logger.error("Failed to insert PlayerHistory", "playerController => insertPlayerReward()", 5);
                    let apiResponse = response.generate(true, "Failed to insert PlayerHistory", 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(tokenDetails)) {
                    logger.error("competition not found", "playerController => findPlayers()", 5);
                    let apiResponse = response.generate(true, "competition not found", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(tokenDetails);
                }
            });
        });
    } // end of findPlayers function

    let findPlayerWallet = (data) => {
        console.log("findPlayerWallet");
        return new Promise((resolve, reject) => {
            Wallet.findOne({playerId: req.body.playerId}, function (err, WalletDetails) {
                if (err) {
                    logger.error("Failed to find Wallet", "playerController => insertPlayerReward()", 5);
                    let apiResponse = response.generate(true, "Failed to find Wallet", 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(WalletDetails)) {
                    logger.error("Wallet not found", "playerController => findPlayers()", 5);
                    let apiResponse = response.generate(true, "Wallet not found", 500, null);
                    reject(apiResponse);
                } else {
                    let final = {
                        entryAmount: data.entryAmount,
                        wallet: WalletDetails.toObject()
                    }
                    resolve(final);
                }
            });
        });
    } // end of findPlayerWallet function

    let createTransaction = (body) => {
        console.log("createTransaction");
        return new Promise((resolve, reject) => {
            body['transactionId'] = shortid.generate();
            body['createdOn'] = new Date();
            Transaction.create(body, function (err, TransactionDetails) {
                if (err) {
                    logger.error("Failed to create Transaction", "playerController => insertPlayerReward()", 5);
                    let apiResponse = response.generate(true, "Failed to create Transaction", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(TransactionDetails);
                }
            });
        });
    } // end of findPlayerWallet function

    let updateWallet = (data) => {
        console.log("updateWallet");
        return new Promise((resolve, reject) => {
            let deductAmount = data.deductAmount;
            data.totalCashBalance -= deductAmount;
            delete data['deductAmount'];
            Wallet.findOneAndUpdate({playerId: data.playerId}, data, {returnOriginal: false})
                .then((res) => {
                    resolve(res);
                })
                .catch(err => {
                    console.log('=> an error occurred: ', err);
                    reject({statusCode: 500, body: 'error'});
                });
        })
    } // end of updateWallet function

    let processData = (data) => {
        console.log("processData");
        return new Promise((resolve, reject) => {
            console.log('data', data);
            let finalRes = {
                bonusCashBalance: 0,
                winningCashBalance: 0,
                depositCashBalance: 0
            };

            let playerWallet = data.wallet;

            let winningAmountLeft = Number(data.entryAmount);

            let firstCutOff = Number((winningAmountLeft * 20) / 100);

            if (firstCutOff > (playerWallet.bonusCashBalance)) {
                winningAmountLeft -= (playerWallet.bonusCashBalance);
                finalRes.bonusCashBalance = (playerWallet.bonusCashBalance);
                (playerWallet.bonusCashBalance) = 0;
            } else {
                (playerWallet.bonusCashBalance) -= ((winningAmountLeft * 20) / 100);
                finalRes.bonusCashBalance = ((winningAmountLeft * 20) / 100);
                winningAmountLeft -= ((winningAmountLeft * 20) / 100);
            }
            console.log('(playerWallet.bonusCashBalance), winningAmountLeft', (playerWallet.bonusCashBalance), winningAmountLeft);

            if (winningAmountLeft > playerWallet.depositCashBalance) {
                winningAmountLeft -= playerWallet.depositCashBalance;
                finalRes.depositCashBalance = playerWallet.depositCashBalance;
                playerWallet.depositCashBalance = 0;
            } else {
                playerWallet.depositCashBalance = playerWallet.depositCashBalance - winningAmountLeft;
                finalRes.depositCashBalance = winningAmountLeft;
                winningAmountLeft = 0
            }
            console.log('playerWallet.depositCashBalance, winningAmountLeft', playerWallet.depositCashBalance, winningAmountLeft);

            if (winningAmountLeft > playerWallet.winningCashBalance) {
                winningAmountLeft = winningAmountLeft - playerWallet.winningCashBalance;
                finalRes.winningCashBalance = playerWallet.winningCashBalance;
                playerWallet.winningCashBalance = 0;
                reject({
                    statusCode: 500,
                    body: 'error',
                    message: 'You Dont Have sufficient balance. Add balance in your wallet'
                });
            } else {
                playerWallet.winningCashBalance = playerWallet.winningCashBalance - winningAmountLeft;
                finalRes.winningCashBalance = winningAmountLeft;
                winningAmountLeft = 0;
                playerWallet.deductAmount = Number(data.entryAmount);
                finalRes.deductAmount = Number(data.entryAmount);
                updateWallet(playerWallet)
                    .then(() => {
                        resolve(finalRes);
                    })
                    .catch((err) => {
                        console.log('error', err);
                        reject(err)
                    });
            }
            console.log('playerWallet.winningCashBalance, winningAmountLeft', playerWallet.winningCashBalance, winningAmountLeft);

        });
    }

    let insertCompetitionEntry = (data) => {
        console.log("insertCompetitionEntry", data);
        return new Promise((resolve, reject) => {
            let body = {};
            body['ceId'] = shortid.generate();
            body['playerId'] = req.body.playerId;
            body['competitionId'] = req.body.competitionId;
            body['depositCashBalance'] = data.depositCashBalance;
            body['winningCashBalance'] = data.winningCashBalance;
            body['bonusCashBalance'] = data.bonusCashBalance;
            body['entryAmount'] = data.deductAmount;
            body['createdOn'] = new Date();
            CompetitionEntry.create(body, function (err, tokenDetails) {
                if (err) {
                    logger.error("Failed to insert PlayerHistory", "playerController => insertPlayerReward()", 5);
                    let apiResponse = response.generate(true, "Failed to insert PlayerHistory", 500, null);
                    reject(apiResponse);
                } else {
                    let newBody = {
                        playerId: req.body.playerId,
                        currencyType: 'withdraw',
                        currencyAmount: data.deductAmount,
                        transactionType: 'join-competition',
                        transactionName: 'Withdraw amount from wallet for joining Competition'
                    }
                    createTransaction(newBody)
                        .then(() => {
                            resolve(tokenDetails);
                        })
                }
            });
        });
    } // end of findPlayers function

    validatingInputs()
        .then(findCompetition)
        .then(findPlayerWallet)
        .then(processData)
        .then(insertCompetitionEntry)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Transaction created Successfully!!", 200, resolve);
            console.log('res', apiResponse);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of getCompetitionData function


module.exports = {
    createRecord: createRecord
}// end exports
