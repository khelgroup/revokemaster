import {Routes} from '@angular/router';

import {HomeComponent} from '../../home/home.component';
import {ConfigurationComponent} from '../../configuration/configuration.component';
import {TransactionComponent} from '../../transaction/transaction.component';
import {CompetitionsComponent} from '../../competitions/competitions.component';
import {PlayersComponent} from '../../players/players.component';
import {RevokeHistoryComponent} from "../../revoke-history/revoke-history.component";


export const AdminLayoutRoutes: Routes = [
    {path: 'dashboard', component: HomeComponent},
    {path: 'competition', component: CompetitionsComponent},
    {path: 'players', component: PlayersComponent},
    {path: 'transaction', component: TransactionComponent},
    {path: 'revokehistory', component: RevokeHistoryComponent},
];
