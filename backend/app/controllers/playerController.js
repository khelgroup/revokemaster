const mongoose = require('mongoose');
const shortid = require('shortid');
const response = require('./../libs/responseLib');
const logger = require('./../libs/loggerLib');
const check = require('../libs/checkLib');

const Player = mongoose.model('Player');
const Wallet = mongoose.model('Wallet');
const Transaction = mongoose.model('transaction');
const RevokeHistory = mongoose.model('revokeHistory');

let list = (req, res) => {

    // get size Limit
    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.body.pgSize) ? Number(req.body.pgSize) : 5;
            if (req.body.pg > 0) {
                skip = (limit) * (req.body.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    let findPlayers = (data) => {
        console.log("findPlayers");
        return new Promise((resolve, reject) => {
            let body = {};
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.id !== undefined) {
                    body['playerId'] = (req.body.data.id);
                }
                if (req.body.data.number !== undefined) {
                    body['mobileNumber'] = (req.body.data.number);
                }
            }
            console.log('body', body);
            Player.find(body, {}, {
                skip: data.skip,
                limit: data.limit,
                sort: {createdOn: -1}
            })
                .select('-__v -_id')
                .exec((err, playerDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve players", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(playerDetails)) {
                        logger.error("No User found", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "No User found with this userId", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(playerDetails);
                    }
                })
        });
    }

    let findPlayerscount = (data) => {
        console.log("findPlayerscount");
        return new Promise((resolve, reject) => {
            let body = {};
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.id !== undefined) {
                    body['playerId'] = (req.body.data.id);
                }
                if (req.body.data.number !== undefined) {
                    body['mobileNumber'] = (req.body.data.number);
                }
            }
            console.log('body', body);
            Player.find(body)
                .count()
                .exec((err, rewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = {
                            results: data,
                            count: rewardsDetails
                        }
                        resolve(final);
                    }
                });
        });
    } // end of findPlayers function

    getSizeLimit()
        .then(findPlayers)
        .then(findPlayerscount)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Player List Successfully!!", 200, resolve);
            console.log('res', apiResponse);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of list function

let updatePlayerWallet = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.playerId && req.body.type && req.body.amount) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "playerId or type or amount is missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let findPlayer = (data) => {
        console.log("findPlayer");
        return new Promise((resolve, reject) => {
            Player.find({playerId: req.body.playerId}, {}, {})
                .select('-__v -_id')
                .exec((err, playerDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve players", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(playerDetails)) {
                        logger.error("No Player found", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "No player found with this userId", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(playerDetails);
                    }
                })
        });
    }

    let createTransaction = (body) => {
        console.log("createTransaction");
        return new Promise((resolve, reject) => {
            body['transactionId'] = shortid.generate();
            body['createdOn'] = new Date();
            Transaction.create(body, function (err, TransactionDetails) {
                if (err) {
                    logger.error("Failed to create Transaction", "playerController => insertPlayerReward()", 5);
                    let apiResponse = response.generate(true, "Failed to create Transaction", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(TransactionDetails);
                }
            });
        });
    } // end of findPlayerWallet function

    let findAndUpdateWallet = () => {
        console.log("findAndUpdateWallet");
        return new Promise((resolve, reject) => {
            let body = {};
            body[req.body.type] = req.body.amount;
            body['totalCashBalance'] = req.body.amount;
            Wallet.findOneAndUpdate({playerId: req.body.playerId}, {"$inc": body}, {returnOriginal: false})
                .exec((err, playerDetails) => {
                    if (err) {
                        logger.error("Failed to Update Wallet", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Failed to Update Wallet", 500, null);
                        reject(apiResponse);
                    } else {
                        let newBody = {
                            playerId: req.body.playerId,
                            currencyType: 'deposit',
                            currencyAmount: req.body.amount,
                            transactionType: 'deposit',
                            transactionName: 'Deposit Amount from Admin Side'
                        }
                        createTransaction(newBody)
                            .then(() => {
                                resolve(playerDetails);
                            })
                    }
                })
        });
    }

    let revokeHistory = () => {
        console.log("revokeHistory");
        return new Promise((resolve, reject) => {
            let body = {};
            body['revokeHistoryId'] = shortid.generate();
            body['playerId'] = req.body.playerId;
            body['transactionAmount'] = req.body.amount;
            body['description'] = 'Money Revoke from Admin Side';
            body['createdOn'] = new Date();
            RevokeHistory.create(body, function (err, rewardsDetails) {
                if (err) {
                    console.log('err', err)
                    logger.error("Failed to Create", "playerController => insertPlayerReward()", 5);
                    let apiResponse = response.generate(true, "Failed to Create", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(rewardsDetails);
                }
            });
        });
    } // end of findPlayers function

    validatingInputs()
        .then(findPlayer)
        .then(findAndUpdateWallet)
        .then(revokeHistory)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Update Wallet Successfully!!", 200, resolve);
            console.log('res', apiResponse);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of updatePlayerWallet function

let deductPlayerWallet = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.playerId && req.body.type && req.body.amount) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "playerId or type or amount is missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let findPlayer = (data) => {
        console.log("findPlayer");
        return new Promise((resolve, reject) => {
            Player.find({playerId: req.body.playerId}, {}, {})
                .select('-__v -_id')
                .exec((err, playerDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve players", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(playerDetails)) {
                        logger.error("No Player found", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "No player found with this userId", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(playerDetails);
                    }
                })
        });
    }

    let createTransaction = (body) => {
        console.log("createTransaction");
        return new Promise((resolve, reject) => {
            body['transactionId'] = shortid.generate();
            body['createdOn'] = new Date();
            Transaction.create(body, function (err, TransactionDetails) {
                if (err) {
                    logger.error("Failed to create Transaction", "playerController => insertPlayerReward()", 5);
                    let apiResponse = response.generate(true, "Failed to create Transaction", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(TransactionDetails);
                }
            });
        });
    } // end of findPlayerWallet function

    let findAndUpdateWallet = () => {
        console.log("findAndUpdateWallet");
        return new Promise((resolve, reject) => {
            let body = {};
            let type = {};
            type[req.body.type] = {
                 '$gte': req.body.amount
            };
            body[req.body.type] = Number(-req.body.amount);
            body['totalCashBalance'] = Number(-req.body.amount);
            console.log('body',body, type)
            Wallet.findOneAndUpdate({$and : [{playerId: req.body.playerId}, type]}, {"$inc": body }, {returnOriginal: false})
                .exec((err, playerDetails) => {
                    console.log('playerDetails',playerDetails)
                    if (err) {
                        logger.error("Failed to Update Wallet", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Failed to Update Wallet", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(playerDetails)) {
                        logger.error("Wallet doesn't have enough money to withdraw", "playerController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Wallet doesn't have enough money to withdraw", 500, null);
                        reject(apiResponse);
                    }else {
                        let newBody = {
                            playerId: req.body.playerId,
                            currencyType: 'withdraw',
                            currencyAmount: req.body.amount,
                            transactionType: 'withdraw',
                            transactionName: 'Withdraw Amount from Admin Side'
                        }
                        createTransaction(newBody)
                            .then(() => {
                                resolve(playerDetails);
                            })
                    }
                })
        });
    }

    let revokeHistory = () => {
        console.log("revokeHistory");
        return new Promise((resolve, reject) => {
            let body = {};
            body['revokeHistoryId'] = shortid.generate();
            body['playerId'] = req.body.playerId;
            body['transactionAmount'] = req.body.amount;
            body['description'] = 'Money Re-Revoke by Admin';
            body['createdOn'] = new Date();
            RevokeHistory.create(body, function (err, rewardsDetails) {
                if (err) {
                    console.log('err', err)
                    logger.error("Failed to Create", "playerController => insertPlayerReward()", 5);
                    let apiResponse = response.generate(true, "Failed to Create", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(rewardsDetails);
                }
            });
        });
    } // end of findPlayers function

    validatingInputs()
        .then(findPlayer)
        .then(findAndUpdateWallet)
        .then(revokeHistory)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Update Wallet Successfully!!", 200, resolve);
            console.log('res', apiResponse);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of deductPlayerWallet function

module.exports = {
    list: list,
    updatePlayerWallet: updatePlayerWallet,
    deductPlayerWallet: deductPlayerWallet,
}// end exports
