import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {LbdModule} from '../../lbd/lbd.module';
import {NguiMapModule} from '@ngui/map';

import {AdminLayoutRoutes} from './admin-layout.routing';
import {HomeComponent} from '../../home/home.component';
import {IconsComponent} from '../../icons/icons.component';
import {NotificationsComponent} from '../../notifications/notifications.component';
import {ConfigurationComponent} from '../../configuration/configuration.component';
import {TransactionComponent} from '../../transaction/transaction.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PlayersComponent} from '../../players/players.component';
import {CompetitionsComponent} from '../../competitions/competitions.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {RevokeHistoryComponent} from "../../revoke-history/revoke-history.component";
import {NgxDaterangepickerMd} from "ngx-daterangepicker-material";


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminLayoutRoutes),
        FormsModule,
        LbdModule,
        NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'}),
        NgxDatatableModule,
        NgbModule,
        NgxDaterangepickerMd
    ],
    declarations: [
        HomeComponent,
        IconsComponent,
        NotificationsComponent,
        ConfigurationComponent,
        TransactionComponent,
        PlayersComponent,
        CompetitionsComponent,
        RevokeHistoryComponent
    ]
})

export class AdminLayoutModule {
}
