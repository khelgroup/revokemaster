const express = require('express');
const router = express.Router();
const competitionController = require("./../../app/controllers/competitionController");
const appConfig = require("./../../config/appConfig");

module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/competition`;

    // defining routes.

    // create player
    app.post(`${baseUrl}/`, competitionController.getCompetitionData);

    app.get(`${baseUrl}/getTransaction`, competitionController.getTransaction);
}
