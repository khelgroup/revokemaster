import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevokeHistoryComponent } from './revoke-history.component';

describe('RevokeHistoryComponent', () => {
  let component: RevokeHistoryComponent;
  let fixture: ComponentFixture<RevokeHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevokeHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevokeHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
