import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../../app/services/notification.service';
import {ConfigurationService} from '../../app/services/configuration.service';
import {cursorTo} from 'readline';

@Component({
    selector: 'app-configuration',
    templateUrl: './configuration.component.html',
    styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

    rows: any = [];
    config: any = [];
    currentPage = 0;
    pageSize = 5;
    rewardName = 'name';
    rewardType = 'point';
    totalSize;
    display = 'none';
    editBlock = false;
    configurationData: any = {};
    limitOptions = [
        {
            key: '5',
            value: 5
        },
        {
            key: '10',
            value: 10
        },
        {
            key: '20',
            value: 20
        }
    ];
    typeOptions = [
        {
            key: 'point',
            value: 'point'
        },
        {
            key: 'coins',
            value: 'coins'
        },
        {
            key: 'cash',
            value: 'cash'
        }
    ];
    menuOptions = [
        {
            key: 'name',
            value: 'name'
        },
        {
            key: 'dob',
            value: 'dob'
        },
        {
            key: 'kyc',
            value: 'kyc'
        },
        {
            key: 'other',
            value: 'other'
        }
    ];

    constructor(private notificationService: NotificationService,
                private configurationService: ConfigurationService) {
    }

    ngOnInit() {
        this.getRewards(this.currentPage, this.pageSize);
    }

    getRewards(pageNumber, pageSize) {
        let userId = localStorage.getItem('userId')
        this.configurationService.getAdminRewards(pageNumber, pageSize, userId)
            .subscribe((response) => {
                    if (!response.error) {
                        this.rows = response.data.results;
                        this.totalSize = response.data.count;
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    addConfiguration() {
        this.editBlock = false;
        this.display = 'block';
    }

    onCloseHandled() {
        this.display = 'none';
        this.configurationData = {};
    }

    saveConfiguration() {
        this.display = 'none';
        let userid = localStorage.getItem('userId');
        this.configurationData['userId'] = userid;
        console.log('configurationData', this.configurationData);
        let ary = this.configurationData.keywords.split(',')
        this.configurationData.keywords = ary;
        this.configurationService.addRewards(this.configurationData)
            .subscribe((response) => {
                    if (!response.error) {
                        this.notificationService.showNotification(response.message, 'success');
                        this.getRewards(this.currentPage, this.pageSize);
                        this.configurationData = {};
                    } else {
                        this.notificationService.showNotification(response.message, 'danger');
                        this.configurationData = {};
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                    this.configurationData = {};
                })
    }

    editConfiguration(data) {
        console.log('edit', data);
        this.display = 'block';
        this.configurationData = data;
        this.editBlock = true;
    }

    onPageSizeChanged(event) {
        this.currentPage = 0;
        this.pageSize = event;
        this.getRewards(this.currentPage, this.pageSize);
    }

    onPageMenuChanged(event) {
        this.rewardName = event;
    }

    onPageTypeChanged(event) {
        this.rewardType = event;
    }

    pageCallback(e) {
        this.getRewards(e.offset, e.pageSize);
    }

    deleteConfig(data) {
        console.log('delete', data.configurationId);
        let value = {
            configurationId: data.configurationId
        };
        this.configurationService.deleteConfiguration(value)
            .subscribe((response) => {
                    if (!response.error) {
                        this.notificationService.showNotification(response.message, 'success');
                        this.getRewards(this.currentPage, this.pageSize);
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }
}
