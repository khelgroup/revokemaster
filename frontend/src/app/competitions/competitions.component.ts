import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../services/notification.service';
import {ConfigurationService} from '../services/configuration.service';

import * as moment from 'moment';
import {NgbCalendar, NgbDate} from '@ng-bootstrap/ng-bootstrap';
import {Moment} from 'moment';

@Component({
    selector: 'app-competitions',
    templateUrl: './competitions.component.html',
    styleUrls: ['./competitions.component.scss']
})
export class CompetitionsComponent implements OnInit {

    rows: any = [];
    details: any = [];
    config: any = [];
    currentPage = 0;
    detailscurrentPage = 0;
    pageSize = 5;
    rewardName = 'name';
    rewardType = 'point';
    totalSize;
    detailstotalSize;
    display = 'none';
    editBlock = false;
    configurationData: any = {};
    selectedDate: { start: Moment, end: Moment };
    limitOptions = [
        {
            key: '5',
            value: 5
        },
        {
            key: '10',
            value: 10
        },
        {
            key: '20',
            value: 20
        }
    ];
    filterData: any = {};

    constructor(private notificationService: NotificationService,
                private configurationService: ConfigurationService) {
    }

    ngOnInit() {
        this.getCompetitionData(this.currentPage, this.pageSize);
    }

    getCompetitionData(pageNumber, pageSize) {
        const userId = localStorage.getItem('userId');
        let data = {
            pg: pageNumber,
            pgSize: pageSize,
            data: this.filterData
        };
        this.configurationService.getCompetitionData(data)
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response.data);
                        this.rows = response.data.results;
                        this.totalSize = response.data.count;
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    filterMethod() {
        if (this.selectedDate.start !== null) {
            this.filterData.startDate = moment(this.selectedDate.start).format('YYYY-MM-DD');
        }
        if (this.selectedDate.end !== null) {
            this.filterData.endDate = moment(this.selectedDate.end).format('YYYY-MM-DD');
        }
        this.getCompetitionData(this.currentPage, this.pageSize);
    }

    detailData(pageNumber, pageSize, data) {
        this.configurationService.getTransactionDataById(pageNumber, pageSize, data.competitionId, 'CO')
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response.data);
                        this.details = response.data.results;
                        this.detailstotalSize = response.data.count;
                    } else {
                        this.editBlock = false;
                        this.display = 'none';
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    showDetails(data) {
        this.editBlock = false;
        this.display = 'block';
        this.detailData(this.detailscurrentPage, this.pageSize, data)
    }

    onCloseHandled() {
        this.display = 'none';
        this.configurationData = {};
    }

    onPageSizeChanged(event) {
        this.currentPage = 0;
        this.pageSize = event;
        this.getCompetitionData(this.currentPage, this.pageSize);
    }

    pageCallback(e) {
        this.getCompetitionData(e.offset, e.pageSize);
    }

    detailPageCallback(e, row) {
        console.log('e.competitionId', e);
        console.log('e.competitionId', row[0].competitionId);
        this.detailData(e.offset, e.pageSize, row[0]);
    }

    revokeData(row) {
        console.log('row', row);
        const body = {
            competitionId: row.competitionId,
            playerId: row.playerId,
            ceId: row.ceId,
            entryAmount: row.entryAmount,
            depositCashBalance: row.depositCashBalance,
            bonusCashBalance: row.bonusCashBalance,
            winningCashBalance: row.winningCashBalance
        }
        this.configurationService.revokeData(body)
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response.data);
                        this.notificationService.showNotification(response.message, 'success')
                        // this.display = 'none'
                        this.detailData(this.detailscurrentPage, this.pageSize, row);
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    revokeallData() {
        this.configurationService.revokeallData(this.details)
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('response.data.results', response.data);
                        this.notificationService.showNotification(response.message, 'success')
                        this.detailData(this.detailscurrentPage, this.pageSize, this.details[0])
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

}
