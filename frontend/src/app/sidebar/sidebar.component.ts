import {Component, OnInit} from '@angular/core';

declare const $: any;

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}


export const ROUTES: RouteInfo[] = [
    {path: '/competition', title: 'Competition', icon: 'pe-7s-graph', class: ''},
    {path: '/players', title: 'Players', icon: 'pe-7s-user', class: ''},
    {path: '/transaction', title: 'Transaction', icon: 'pe-7s-note2', class: ''},
    {path: '/revokehistory', title: 'Revoke History', icon: 'pe-7s-note2', class: ''},
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
    menuItems: any[];
    userRole;

    constructor() {
    }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
}
