'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let WalletCollectionSchema = new Schema({
    walletId: {
        type: String,
        default: '',
        index: true,
        unique: true
    },
    // currencyType: {
    //     type: String,
    //     default: ''
    // },
    bonusCashBalance: {
        type: Number,
        default: 0
    },
    winningCashBalance: {
        type: Number,
        default: 0
    },
    totalCashBalance: {
        type: Number,
        default: 0
    },
    depositCashBalance: {
        type: Number,
        default: 0
    },
    playerId: {
        type: String,
        default: '',
        index: true
    },
    totalTokenBalance: {
        type: Number,
        default: 0
    },
    productId: {
        type: String,
        default: ''
    },
    createdOn: {
        type: Date,
        default: ''
    }
}, { versionKey: false  })


mongoose.model('Wallet', WalletCollectionSchema);
