'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let transactionCollectionSchema = new Schema({
    transactionId: {
        type: String,
        default: '',
        index: true,
        unique: true
    },
    currencyType: {
        type: String,
        default: ''
    },
    currencyAmount: {
        type: Number,
        default: 0
    },
    transactionName: {
        type: String,
        default: ''
    },
    transactionType: {
        type: String,
        default: ''
    },
    transactionStatus: {
        type: String,
        default: 0
    },
    currentStatus: {
        type: Boolean,
        default: false
    },
    failureReason: {
        type: String,
        default: ''
    },
    playerId: {
        type: String,
        default: '',
        index: true
    },
    createdOn: {
        type: Date,
        default: ''
    }
}, { versionKey: false  })


mongoose.model('transaction', transactionCollectionSchema);
