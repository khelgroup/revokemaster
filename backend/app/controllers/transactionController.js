const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib');
const logger = require('./../libs/loggerLib');
const check = require('../libs/checkLib');
const tokenLib = require('../libs/tokenLib');

var faker = require('faker');

/* Models */

const CompetitionEntry = mongoose.model('CompetitionEntry');
const Transaction = mongoose.model('transaction');
const Competition = mongoose.model('competition');
const Player = mongoose.model('Player');
const Wallet = mongoose.model('Wallet');

let getFilterTransaction = (req, res) => {
    // get size Limit
    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.body.pgSize) ? Number(req.body.pgSize) : 5;
            if (req.body.pg > 0) {
                skip = (limit) * (req.body.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    let findTransaction = (data) => {
        console.log("findTransaction");
        return new Promise((resolve, reject) => {
            let body = {};
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.startDate && req.body.data.endDate) {
                    body["createdOn"] = {
                        "$gte": new Date(req.body.data.startDate),
                        "$lt": new Date(req.body.data.endDate)
                    }
                }
                if (req.body.data.amountFilter) {
                    body["currencyAmount"] = {"$gte": Number(req.body.data.amountFilter)}
                }
            }
            console.log('filter body', body);
            Transaction.find(body, {}, {
                skip: data.skip,
                limit: data.limit,
                sort: {createdOn: -1}
            })
                .select('-__v -_id')
                .sort({createdOn: -1})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("Transaction not found for this competition or player", "playerController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Transaction not found for this competition or player", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findTransaction function

    let findPlayerName = (data) => {
        console.log("findPlayerName");
        return new Promise((resolve, reject) => {
            let playerIds = [];
            data.filter((x) => {
                if (playerIds.indexOf(x.playerId) === -1) {
                    playerIds.push(x.playerId);
                }
            });
            Player.find({playerId: {"$in": playerIds}})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("Player Details not found", "playerController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Player Details not found", 500, null);
                        reject(apiResponse);
                    } else {
                        let result = [];
                        rewardsDetails.filter((x) => {
                            x = x.toObject();
                            data.filter((y) => {
                                y = y.toObject();
                                if (x.playerId === y.playerId) {
                                    y['profileName'] = x.profileName;
                                    result.push(y);
                                }
                            });
                        });
                        result.sort(function(a, b){return b.createdOn - a.createdOn});
                        resolve(result);
                    }
                });
        });
    } // end of transactionCount function

    // let findCompetitionEntry = (data) => {
    //     console.log("findCompetitionName");
    //     return new Promise((resolve, reject) => {
    //         let competitionIds = [];
    //         data.filter((x) => {
    //             if (competitionIds.indexOf(x.competitionId) === -1) {
    //                 competitionIds.push(x.competitionId);
    //             }
    //         });
    //         Competition.find({competitionId: {"$in": competitionIds}})
    //             .exec((err, rewardsDetails) => {
    //                 if (err) {
    //                     logger.error("Failed to find PlayerHistory count", "playerController => insertPlayerReward()", 5);
    //                     let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
    //                     reject(apiResponse);
    //                 } else if (check.isEmpty(rewardsDetails)) {
    //                     logger.error("Competition Details not found", "playerController => findPlayers()", 5);
    //                     let apiResponse = response.generate(true, "Competition Details not found", 500, null);
    //                     reject(apiResponse);
    //                 } else {
    //                     let result = [];
    //                     rewardsDetails.filter((x) => {
    //                         x = x.toObject();
    //                         data.filter((y) => {
    //                             if (x.competitionId === y.competitionId) {
    //                                 y['competitionName'] = x.competitionName;
    //                                 result.push(y);
    //                             }
    //                         });
    //                     });
    //                     resolve(result);
    //                 }
    //             });
    //     });
    // } // end of findCompetitionName function

    let transactionCount = (data) => {
        console.log("transactionCount");
        return new Promise((resolve, reject) => {
            let body = {};
            if (Object.keys(req.body.data).length > 0) {
                if (req.body.data.startDate && req.body.data.endDate) {
                    body["createdOn"] = {
                        "$gte": new Date(req.body.data.startDate),
                        "$lt": new Date(req.body.data.endDate)
                    }
                }
                if (req.body.data.amountFilter) {
                    body["currencyAmount"] = {"$gte": Number(req.body.data.amountFilter)}
                }
            }
            console.log('filter body', body);
            Transaction.find(body)
                .count()
                .exec((err, rewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("Transaction not found", "playerController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Transaction not found", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = {
                            results: data,
                            count: rewardsDetails
                        }
                        resolve(final);
                    }
                });
        });
    } // end of transactionCount function

    getSizeLimit()
        .then(findTransaction)
        .then(findPlayerName)
        // .then(findCompetitionName)
        .then(transactionCount)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Transaction created Successfully!!", 200, resolve);
            console.log('res', apiResponse);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of getFilterTransaction function

let transactionRevoke = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.query.transactionId && req.query.playerId && req.query.currencyAmount) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "transactionId or playerId or currencyAmount is missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let findTransaction = () => {
        console.log("findTransaction");
        return new Promise((resolve, reject) => {
            Transaction.findOne({transactionId: req.query.transactionId}, {}, {})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to find Transaction", "transactionController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "Failed to find Transaction", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("No Transaction found", "transactionController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "No Transaction found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findTransaction function

    let findPlayer = () => {
        console.log("findPlayer");
        return new Promise((resolve, reject) => {
            Player.findOne({playerId: req.query.playerId}, {}, {})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to find Player", "transactionController => findPlayer()", 5);
                        let apiResponse = response.generate(true, "Failed to find Player", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("No Player found", "transactionController => findPlayer()", 5);
                        let apiResponse = response.generate(true, "No Player found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findPlayers function

    let updateWallet = () => {
        console.log("updateWallet");
        return new Promise((resolve, reject) => {
            Wallet.findOneAndUpdate({playerId: req.query.playerId}, {
                $inc: {
                    totalCashBalance: req.query.currencyAmount
                }
            }, {new: true})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to update wallet", "transactionController => updateWallet()", 5);
                        let apiResponse = response.generate(true, "Failed to update wallet", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of UpdateWallet function

    let updateTransaction = () => {
        console.log("updateWallet");
        return new Promise((resolve, reject) => {
            const update = {currentStatus: true};
            Transaction.findOneAndUpdate({transactionId: req.query.transactionId}, update, {new: true})
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to update Transaction", "transactionController => updateTransaction()", 5);
                        let apiResponse = response.generate(true, "Failed to update Transaction", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of UpdateWallet function

    validatingInputs()
        .then(findTransaction)
        .then(findPlayer)
        .then(updateWallet)
        .then(updateTransaction)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Transaction Revoke Successfully!!", 200, resolve);
            console.log('res', apiResponse);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of transactionRevoke function

module.exports = {
    getFilterTransaction: getFilterTransaction,
    transactionRevoke: transactionRevoke,
}// end exports
