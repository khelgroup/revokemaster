'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let revokeHistorySchema = new Schema({
    revokeHistoryId: {
        type: String,
        default: '',
// enables us to search the record faster
        index: true,
        unique: true
    },
    playerId: {
        type: String,
        default: ''
    },
    competitionId: {
        type: String,
        default: ''
    },
    transactionId: {
        type: String,
        default: ''
    },
    transactionAmount: {
        type: String,
        default: ''
    },
    description: {
        type: String,
        default: ''
    },
    createdOn: {
        type: Date,
        default: ''
    },
}, {versionKey: false});

mongoose.model('revokeHistory', revokeHistorySchema);
